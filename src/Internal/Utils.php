<?php

namespace Passport\Internal;

use Passport\API\Exceptions\PassportException;
use Passport\Internal\Http\HttpRequestBuilderHolder;
use Passport\Internal\Exceptions\PassportExceptionImpl;
use Passport\Settings;

/**
 * Class Utils - ��������������� �����
 * @package Passport\Internal
 */
class Utils
{
    /**
     * ����������� ������� ����������� � ������� xsd duration � �������
     * @param string $period
     * @return int
     */
    static function periodToSeconds($period)
    {
        $period = str_replace('.0S', 'S', $period);
        $interval = new \DateInterval($period);
        return date_create('@0')->add($interval)->getTimestamp();
    }

    static function genUUID()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @param string $soap_url
     * @param array $headers
     * @param string $soap_message
     * @return \SimpleXMLElement
     * @throws PassportException
     */
    static function soapRequest($soap_url, $user_agent, $headers, $soap_message)
    {
        $curl = HttpRequestBuilderHolder::get($soap_url, $user_agent);
        $curl->setOption(CURLOPT_POST, true);
        $curl->setOption(CURLOPT_POSTFIELDS, $soap_message);

        $curl->setOption(CURLOPT_RETURNTRANSFER, 1);

        foreach($headers as $header) {
            $curl->addHeader($header);
        }

        $response = $curl->execute();
        $curl->close();

        try {
            $xml = simplexml_load_string($response);
            if ($xml === FALSE) {
                throw new PassportExceptionImpl('Invalid server response');

            }
            return $xml;
        } catch (\Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }

    static function getHeadersFromCurlResponse($response)
    {
        $headers = array();

        $l_pos = strpos($response, '200 OK');
        if($l_pos == false)
            $l_pos = strlen('HTML/1.1 ');

        $response = substr($response, $l_pos, strlen($response));

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0)
                $headers['http_code'] = $line;
            else {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

        return $headers;
    }

    static function getRespCode($resp_line)
    {
        list ($resp_code) = explode(' ', $resp_line);
        return (int)$resp_code;
    }

    static function authorize($client_id, $client_secret)
    {
        $soap_url = Settings::$CLIENT_AUTH_URL;

        $soap_message =
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://www.bercut.com/schema/ClientAuth">
               <soapenv:Header/>
               <soapenv:Body>
                  <cli:ClientAuthRequest/>
               </soapenv:Body>
            </soapenv:Envelope>';

        $curl = HttpRequestBuilderHolder::get($soap_url, $client_id);

        $curl->setOption(CURLOPT_POST, true);
        $curl->setOption(CURLOPT_POSTFIELDS, $soap_message);
        $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        $curl->setOption(CURLOPT_HEADER, 1);
        $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        $curl->setOption(CURLOPT_USERPWD, $client_id . ":" . $client_secret);

        $response = $curl->execute();

        $curl->close();

        $headers = Utils::getHeadersFromCurlResponse($response);

        $http_code = $headers['http_code'];
        $resp_code = Utils::getRespCode($http_code);
        if($resp_code != 200)
            throw new PassportExceptionImpl("Auth server has returned $http_code");


        if(!array_key_exists('Set-Cookie', $headers))
            throw new PassportExceptionImpl('No Set-Cookie header in 200 OK response');

        $set_cookie = $headers['Set-Cookie'];
        $guid = substr($set_cookie, strpos($set_cookie, 'guid='), strlen($set_cookie));
        $guid = substr($guid, 5, strpos($guid, '; expires=') - 5);

        return $guid;
    }
}