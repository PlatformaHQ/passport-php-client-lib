<?php

namespace Passport\Internal\Services\Access;

use Passport\API\Common\Token;
use Passport\API\Exceptions\PassportException;
use Passport\API\Services\Access\UserSession;
use Passport\API\Structures\PhoneInfo;
use Passport\API\Structures\ScoringResult;
use Passport\Internal\Clients\UserSessionClient;
use Passport\Internal\Exceptions\PassportExceptionImpl;

/**
 * @package Passport\Internal
 */


class UserSessionImpl extends UserSession
{
    /**
     * @var string
     */
    private $clientId;
    /**
     * @var Token
     */
    private $token;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var PhoneInfo
     */
    private $phoneInfo;

    /**
     * @var ScoringResult
     */
    private $scoringResult;

    /**
     * @deprecated
     * ��. �������� ����������
     * @param Token $token
     * @param string $userId
     * @param string null $phone
     * @param string null $email
     * @throws PassportExceptionImpl
     */
    public function __construct($clientId, $token, $userId, $phone = NULL, $email = NULL)
    {
        if (empty($userId) || empty($token) || empty($clientId)) {
            throw new PassportExceptionImpl("All arguments must be specified");
        }

        if (!($token instanceof Token)) {
            throw new PassportExceptionImpl("Type of  Token must be Token");
        }

        $this->token = $token;
        $this->clientId = $clientId;
        $this->userId = $userId;
        $this->phone = $phone;
        $this->email = $email;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getPhone()
    {
        if (!$this->phone) {
            $this->phone = UserSessionClient::getPhone($this->token->getToken(), $this->clientId);
        }
        return $this->phone;
    }

    public function getEmail()
    {
        if (!$this->email) {
            $this->email = UserSessionClient::getEmail($this->token->getToken(), $this->clientId);
        }
        return $this->email;
    }

    public function getPhoneInfo()
    {
        if (!$this->phoneInfo) {
            $this->phoneInfo = UserSessionClient::getPhoneInfo($this->token->getToken(), $this->clientId);
        }
        return $this->phoneInfo;
    }

    public function setOrder($orderId, $price, $url="empty")
    {
        UserSessionClient::setOrder($this->token->getToken(), $this->clientId, $orderId, $price, $url);
    }

    public function setPayment($order, $payment, $user, $transaction, $price, $status="empty")
    {
        UserSessionClient::setPayment($this->token->getToken(), $this->clientId,
            $order, $payment, $user, $transaction, $price, $status);
    }

    public function getScore()
    {

        if (!$this->scoringResult) {
            $this->scoringResult = UserSessionClient::getScoringResult($this->token->getToken(), $this->clientId);
        }
        return $this->scoringResult;
    }
}