<?php


namespace Passport\Internal\Services\Access;

use Passport\API\Services\Access\UserAccess;
use Passport\Internal\Clients\UserSessionClient;
use Passport\Internal\CommonService;
use Passport\Internal\Exceptions\PassportExceptionImpl;

/**
 * @package Passport\Internal
 */

class UserAccessImpl extends CommonService implements UserAccess
{
    public function newBuilder($callback_url, $data)
    {
        return new UserSessionBuilderImpl($this->client_id, $this->auth_cookie, $callback_url, $data);
    }
}