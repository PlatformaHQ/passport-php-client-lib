<?php

namespace Passport\Internal\Services\Access;

use Passport\API\Common\UserData;
use Passport\API\Services\Access\UserSessionBuilder;
use Passport\Internal\Common\TokenImpl;
use Passport\Internal\Exceptions\PassportExceptionImpl;
use Passport\Internal\Utils;
use Passport\Settings;

/**
 * @package Passport\Internal
 */


class UserSessionBuilderImpl implements UserSessionBuilder
{
    /**
     * @var  string
     */
    private $client_id;

    /**
     * @var  string
     */
    private $auth_cookie;

    /**
     * @var  string
     */
    private $callback_url;

    /**
     * @var  string
     */
    private $state;

    /**
     * @var  UserData
     */
    private $userData;

    /**
     * @deprecated
     * ��. �������� ������
     */
    public function __construct($client_id, $auth_cookie, $callback_url, $data, $state = NULL)
    {
        if (empty($client_id) || empty($auth_cookie) || empty($callback_url) || empty($data)) {
            throw new PassportExceptionImpl("All arguments must be specified");
        }

        if (!($data instanceof UserData)) {
            throw new PassportExceptionImpl("Type of 'data' must be PassportData");
        }

        $this->userData = $data;
        $this->state = ($state === NULL) ? Utils::genUUID() : $state;
        $this->callback_url = urlencode($callback_url);
        $this->client_id = $client_id;
        $this->auth_cookie = $auth_cookie;
    }

    public function getRedirectUrl()
    {
        $params = array('response_type' => 'code',
            'client_id' => $this->client_id,
            'redirect_uri' => $this->callback_url,
            'state' => $this->state,
            'user_data' => $this->userData->getData(),
            //backward compatibility
            'platformaly_data' => $this->userData->getData());

        $redirectUrl = Settings::$AUTH_URL . "?";

        foreach ($params as $key => $val) {
            $redirectUrl .= ($key . "=" . $val . "&");
        }

        return $redirectUrl;
    }

    public function build($code, $state)
    {
        if (empty($code) || empty($state)) {
            throw new PassportExceptionImpl("All arguments must be specified");
        }
        if ($state != $this->state) {
            throw new PassportExceptionImpl("Invalid state");
        }

        return $this->tokenRequestXml($code);
    }

    private function tokenRequestXml($code)
    {
        $message_template = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:auth="http://www.bercut.com/spec/schema/Auth">
               <soapenv:Header/>
               <soapenv:Body>
                  <auth:getTokenRequest>
                     <auth:redirectUri>__REDIRECTURI__</auth:redirectUri>
                     <auth:corrId>__CORRID__</auth:corrId>
                  </auth:getTokenRequest>
               </soapenv:Body>
            </soapenv:Envelope>';

        $mapping = array("__REDIRECTURI__" => $this->callback_url, "__CORRID__" => $code);

        $soap_message = str_replace(array_keys($mapping), array_values($mapping), $message_template);

        $headers = array(
            'SOAPAction: getToken',
            'Cookie: guid=' . $this->auth_cookie,
        );
        $soap_url = Settings::$TOKEN_URL;

        $xml = Utils::soapRequest($soap_url, $this->client_id, $headers, $soap_message);

        try {
            $accessToken = null;
            $accessTokenXml = $xml->xpath("//*[local-name()='accessToken']");
            if (count($accessTokenXml) > 0) {
                $accessToken = strip_tags($accessTokenXml[0]->asXML());
            }

            $expireSeconds = null;
            $expiresIn = null;
            $expiresInXml = $xml->xpath("//*[local-name()='expiresIn']");
            if (count($expiresInXml) > 0) {
                $expiresIn = strip_tags($expiresInXml[0]->asXML());
                $expireSeconds = Utils::periodToSeconds($expiresIn);
            }

            $mobileId = null;
            $mobileIdXml = $xml->xpath("//*[local-name()='mobileId']");
            if (count($mobileIdXml) > 0) {
                $mobileId = strip_tags($mobileIdXml[0]->asXML());
            }

            $userEmail = null;
            $userEmailXml = $xml->xpath("//*[local-name()='userEmail']");
            if (count($userEmailXml) > 0) {
                $userEmail = strip_tags($userEmailXml[0]->asXML());
            }

            $phone = null;
            $phoneXml = $xml->xpath("//*[local-name()='phone']");
            if (count($phoneXml) > 0) {
                $phone = strip_tags($phoneXml[0]->asXML());
            }

            $token = new TokenImpl($accessToken, $expireSeconds);
            $userSession = new UserSessionImpl($this->client_id, $token, $mobileId, $phone, $userEmail);
            return $userSession;

        } catch (\Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }

    }
}