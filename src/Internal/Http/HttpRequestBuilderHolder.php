<?php

namespace Passport\Internal\Http;

/**
 * @package Passport\Internal
 */


class HttpRequestBuilderHolder
{
    /**
     * @var HttpRequestBuilder
     */
    private static $builder = NULL;

    public static function init($proxy = NULL, $credentials = NULL)
    {
        self::$builder = new CurlHttpRequestBuilder($proxy, $credentials);
    }

    /**
     * @param $url
     * @return HttpRequest
     */
    public static function get($url, $user_agent)
    {
        if (self::$builder === NULL) {
            self::$builder = new CurlHttpRequestBuilder(NULL, NULL);
        }
        return self::$builder->build($url, $user_agent);
    }

    public static function set($builder)
    {
        self::$builder = $builder;
    }
}