<?php

namespace Passport\Internal\Http;

/**
 * @package Passport\Internal
 */


class CurlHttpRequest implements HttpRequest
{
    private $handle = null;
    private $headers;

    /**
     * @deprecated
     * использовать @see HttpRequestHolder::get()
     */
    public function __construct($url, $user_agent, $proxy, $credentials)
    {
        $this->handle = curl_init($url);
        if (isset($proxy)) {
            $this->setOption(CURLOPT_PROXY, $proxy);
        }

        if (isset($credentials)) {
            $this->setOption(CURLOPT_PROXYUSERPWD, $credentials);
        }
        $this->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        if(isset($user_agent) && trim($user_agent) !== '') {
            $this->headers = array(
                'User-Agent: ' . $user_agent
            );
        }
    }

    public function setOption($name, $value)
    {
        curl_setopt($this->handle, $name, $value);
    }

    public function addHeader($header) {
        $this->headers[] = $header;
    }

    public function execute()
    {
        $this->setOption(CURLOPT_HTTPHEADER, $this->headers);
        return curl_exec($this->handle);
    }

    public function getInfo($name)
    {
        return curl_getinfo($this->handle, $name);
    }

    public function close()
    {
        curl_close($this->handle);
    }

}