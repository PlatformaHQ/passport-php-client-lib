<?php

namespace Passport\Internal\Http;

/**
 * @package Passport\Internal
 */


class CurlHttpRequestBuilder implements HttpRequestBuilder
{
    private $proxy;
    private $credentials;

    public function __construct($proxy, $credentials)
    {
        $this->proxy = $proxy;
        $this->credentials = $credentials;
    }

    public function build($url, $user_agent)
    {
        return new CurlHttpRequest($url, $user_agent, $this->proxy, $this->credentials);
    }
}