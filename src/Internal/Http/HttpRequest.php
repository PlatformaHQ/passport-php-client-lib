<?php

namespace Passport\Internal\Http;

/**
 * @package Passport\Internal
 */


interface HttpRequest
{
    public function setOption($name, $value);

    public function execute();

    public function getInfo($name);

    public function addHeader($header);

    public function close();

}