<?php

namespace Passport\Internal\Http;

/**
 * @package Passport\Internal
 */


interface HttpRequestBuilder
{
    /**
     * @param string $url
     * @return HttpRequest
     */
    public function build($url, $user_agent);
}