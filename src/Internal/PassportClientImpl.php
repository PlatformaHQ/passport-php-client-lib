<?php

namespace Passport\Internal;

use Passport\API\PassportClient;
use Passport\Internal\Exceptions\PassportExceptionImpl;
use Passport\Internal\Http\HttpRequestBuilderHolder;
use Passport\Internal\Services\Access\UserAccessImpl;

/**
 * @package Passport\Internal
 */


class PassportClientImpl extends PassportClient
{
    /**
     * @var  string
     */
    private $client_id;

    /**
     * @deprecated
     * ��� �������� ���������� ������ ������������ @see PassportClient::getOne()
     */
    public function __construct($client_id)
    {
        if (empty($client_id)) {
            throw new PassportExceptionImpl("client_id must be specified");
        }
        $this->client_id = $client_id;
    }

    public function createUserAccess($client_secret)
    {
        $auth_cookie = Utils::authorize($this->client_id, $client_secret);
        return new UserAccessImpl($this->client_id, $auth_cookie);
    }
}
