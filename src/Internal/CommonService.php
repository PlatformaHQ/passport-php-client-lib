<?php

namespace Passport\Internal;
use Passport\API\Exceptions\PassportException;
use Passport\Internal\Exceptions\PassportExceptionImpl;

/**
 * Class CommonService - ������� ����� �� ��������, �������� ��������������� ������
 * @package Passport\Internal
 */

class CommonService
{
    /**
     * ������������� ���������
     * @var string
    */
    protected $client_id;
    /**
     * ���� �������, ������� ���������� � ����� �� ���� �����/������
     * @var string
     */
    protected $auth_cookie;

    /**
     * @param string $client_id
     * @param string $auth_cookie
     * @throws PassportException
     */
    public function __construct($client_id, $auth_cookie)
    {
        if (empty($client_id)) {
            throw new PassportExceptionImpl("client_id must be specified");
        }
        if (empty($auth_cookie)) {
            throw new PassportExceptionImpl("auth_cookie must be specified");
        }
        $this->client_id = $client_id;
        $this->auth_cookie = $auth_cookie;
    }
}