<?php

namespace Passport\Internal\Exceptions;
use Passport\API\Exceptions\PassportException;

/**
 * Class PassportException - ������� ����������, ������������� ��� ���� ������� ������ ����������
 * @package Passport\Internal
 */
class PassportExceptionImpl extends PassportException
{
    private $type;

    public function __construct($message = '', $type = NULL)
    {
        parent::__construct();
        $this->type = ($type === NULL) ? 'lib_error' : $type;
        $this->message = (strlen($message) > 0) ? $message : 'Unknown error';
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->type}]: {$this->message}\n";
    }
}