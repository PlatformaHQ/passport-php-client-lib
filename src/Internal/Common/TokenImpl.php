<?php

namespace Passport\Internal\Common;

use Passport\API\Common\Token;
use Passport\Internal\Exceptions\PassportExceptionImpl;

/**
 * @package Passport\Internal
 */

class TokenImpl extends  Token
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var \DateTime
     */
    private $expireDate;

    /**
     * @deprecated
     * ��� �������� ���������� ������ ������������ @see Token::getOne()
     */
    public function __construct($token_str = '', $expires_in = NULL)
    {
        if ($token_str == '') {
            throw new PassportExceptionImpl('token_str must be specified');
        }
        $this->token = $token_str;
        $this->expireDate = NULL;

        if (!($expires_in === NULL) && ((int)$expires_in >= 0)) {
            $date = new \DateTime("now");
            $date->add(new \DateInterval("PT" . (string)$expires_in . "S"));
            $this->expireDate = $date;
        }
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getExpireDate()
    {
        return $this->expireDate;
    }
}