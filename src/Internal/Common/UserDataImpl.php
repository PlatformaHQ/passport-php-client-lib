<?php

namespace Passport\Internal\Common;

use Passport\API\Common\UserData;
use Passport\Internal\Exceptions\PassportExceptionImpl;

/**
 * @package Passport\Internal
 */


class UserDataImpl extends UserData
{
    /**
     * @var string
     */
    private $data;

    /**
     * @deprecated
     * ��� �������� ���������� ������ ������������ @see UserData::getOne()
     */
    public function __construct($dataStr)
    {
        if (empty($dataStr)) {
            throw new PassportExceptionImpl("dataStr must be specified");
        }
        $this->data = $dataStr;
    }

    public function getData()
    {
        return $this->data;
    }
}