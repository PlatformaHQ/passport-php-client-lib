<?php

namespace Passport\Internal\Clients;

use Exception;
use Passport\API\Structures\MobileSubscriberData;
use Passport\API\Structures\PhoneInfo;
use Passport\API\Structures\ScoringResult;
use Passport\Internal\Exceptions\PassportExceptionImpl;
use Passport\Internal\Utils;
use Passport\Settings;

/**
 * Class UserSessionClient - ������ ��� UserSession ��������
 * @package Passport\Internal
 */

class UserSessionClient
{
    private static $FIND_SESSION_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession">
                                               <soapenv:Header/>
                                               <soapenv:Body>
                                                  <user:FindSessionRequest>
                                                     <user:token>__TOKEN__</user:token>
                                                  </user:FindSessionRequest>
                                               </soapenv:Body>
                                            </soapenv:Envelope>';

    private static $GET_PHONE_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession">
                                           <soapenv:Header/>
                                           <soapenv:Body>
                                              <user:GetPhoneRequest/>
                                           </soapenv:Body>
                                        </soapenv:Envelope>';

    private static $GET_EMAIL_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession">
                                           <soapenv:Header/>
                                           <soapenv:Body>
                                              <user:GetEmailRequest/>
                                           </soapenv:Body>
                                        </soapenv:Envelope>';

    private static $GET_PHONE_INFO_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession">
                                                   <soapenv:Header/>
                                                   <soapenv:Body>
                                                      <user:GetPhoneInfoRequest/>
                                                   </soapenv:Body>
                                                </soapenv:Envelope>';

    private static $SET_ORDER_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession" xmlns:ns2="http://bercut.com/principalcontext">
                                            <soapenv:Header/>
                                            <soapenv:Body>
                                               <user:SetOrderRequest>
                                                  <user:orderId>__ORDERID__</user:orderId>
                                                  <user:price>__PRICE__</user:price>
                                                  <user:url>__URL__</user:url>
                                               </user:SetOrderRequest>
                                            </soapenv:Body>
                                          </soapenv:Envelope>';

    private static $SET_PAYMENT_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserPayments">
                                               <soapenv:Header/>
                                               <soapenv:Body>
                                                  <user:UserPaymentsRequest>
                                                     <user:order>__ORDER__</user:order>
                                                     <user:payment>__PAYMENT__</user:payment>
                                                     <user:user>__USER__</user:user>
                                                     <user:transaction>__TRANSACTION__</user:transaction>
                                                     <user:price>__PRICE__</user:price>
                                                     <user:status>__STATUS__</user:status>
                                                  </user:UserPaymentsRequest>
                                               </soapenv:Body>
                                            </soapenv:Envelope>';

    private static $GET_SCORING_TEMPLATE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:user="http://www.bercut.com/schema/UserSession">
                                            <soapenv:Header/>
                                            <soapenv:Body>
                                                <user:GetScoringRequest/>
                                            </soapenv:Body>
                                            </soapenv:Envelope>';

    static function getPhoneInfo($token, $user_agent)
    {
        $soap_message = UserSessionClient::$GET_PHONE_INFO_TEMPLATE;
        $headers = array(
            'Cookie: guid=' . $token
        );
        $soap_url = Settings::$USER_SESSION_URL;

        $xml = Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);

        try {
            $mnc = null;
            $mnc_xml = $xml->xpath("//*[local-name()='mnc']");
            if (count($mnc_xml) > 0) {
                $mnc = strip_tags($mnc_xml[0]->asXML());
            }

            $geo_code = null;
            $geo_code_xml = $xml->xpath("//*[local-name()='geoCode']");
            if (count($geo_code_xml) > 0) {
                $geo_code = strip_tags($geo_code_xml[0]->asXML());
            }

            $mcc = null;
            $mcc_xml = $xml->xpath("//*[local-name()='mcc']");
            if (count($mcc_xml) > 0) {
                $mcc = strip_tags($mcc_xml[0]->asXML());
            }

            $operatorName = null;
            $operator_name_xml = $xml->xpath("//*[local-name()='operatorName']");
            if (count($operator_name_xml) > 0) {
                $operatorName = strip_tags($operator_name_xml[0]->asXML());
            }

            $regionName = null;
            $region_name_xml = $xml->xpath("//*[local-name()='regionName']");
            if (count($region_name_xml) > 0) {
                $regionName = strip_tags($region_name_xml[0]->asXML());
            }

            $settlementName = null;
            $settlement_name_xml = $xml->xpath("//*[local-name()='settlementName']");
            if (count($settlement_name_xml) > 0) {
                $settlementName = strip_tags($settlement_name_xml[0]->asXML());
            }

            $fraudSuspicion = null;
            $fraud_suspicion_xml = $xml->xpath("//*[local-name()='fraudSuspicion']");
            if (count($fraud_suspicion_xml) > 0) {
                $fraudSuspicion = strip_tags($fraud_suspicion_xml[0]->asXML());
            }

            $mobileSubscriberData = new MobileSubscriberData();
            $mobileSubscriberData->geo_code = $geo_code;
            $mobileSubscriberData->mcc = $mcc;
            $mobileSubscriberData->mnc = $mnc;

            $phoneInfo = new PhoneInfo();
            $phoneInfo->isFraudSuspicion = $fraudSuspicion;
            $phoneInfo->mobileSubscriberData = $mobileSubscriberData;
            $phoneInfo->operatorName = $operatorName;
            $phoneInfo->regionName = $regionName;
            $phoneInfo->settlementName = $settlementName;

            return $phoneInfo;
        } catch (Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }

    static function getEmail($token, $user_agent)
    {
        $soap_message = UserSessionClient::$GET_EMAIL_TEMPLATE;
        $headers = array(
            'Cookie: guid=' . $token
        );
        $soap_url = Settings::$USER_SESSION_URL;

        $xml = Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);

        try {
            $email = null;
            $email_xml = $xml->xpath("//*[local-name()='email']");
            if (count($email_xml) > 0) {
                $email = strip_tags($email_xml[0]->asXML());
            }
            return $email;
        } catch (Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }

    static function getPhone($token, $user_agent)
    {
        $soap_message = UserSessionClient::$GET_PHONE_TEMPLATE;
        $headers = array(
            'Cookie: guid=' . $token
        );
        $soap_url = Settings::$USER_SESSION_URL;

        $xml = Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);

        try {
            $phone = null;
            $phone_xml = $xml->xpath("//*[local-name()='phoneNumber']");
            if (count($phone_xml) > 0) {
                $phone = strip_tags($phone_xml[0]->asXML());
            }
            return $phone;
        } catch (Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }

    static function findSession($token, $user_agent)
    {
        $mapping = array("__TOKEN__" => $token);
        $soap_message = str_replace(array_keys($mapping), array_values($mapping), UserSessionClient::$FIND_SESSION_TEMPLATE);
        $headers = array(
            'Cookie: guid=' . $token
        );
        $soap_url = Settings::$USER_SESSION_URL;

        $xml = Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);

        try {
            $mobile_id = null;
            $mobile_id_xml = $xml->xpath("//*[local-name()='mobileId']");
            if (count($mobile_id_xml) > 0) {
                $mobile_id = strip_tags($mobile_id_xml[0]->asXML());
            }
            return $mobile_id;
        } catch (Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }

    static function setOrder($token, $user_agent, $orderId, $price, $url)
    {
        $mapping = array("__ORDERID__" => $orderId,
                         "__PRICE__" => $price,
                         "__URL__" => $url);
        $soap_message = str_replace(array_keys($mapping), array_values($mapping), UserSessionClient::$SET_ORDER_TEMPLATE);

        $headers = array(
            'Cookie: guid=' . $token
        );

        $soap_url = Settings::$USER_SESSION_URL;

        Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);
    }

    static function setPayment($token, $user_agent, $order, $payment, $user, $transaction, $price, $status)
    {
        $mapping = array(
            "__ORDER__" => $order,
            "__PAYMENT__" => $payment,
            "__USER__" => $user,
            "__TRANSACTION__" => $transaction,
            "__PRICE__" => $price,
            "__STATUS__" => $status
        );
        $soap_message = str_replace(array_keys($mapping), array_values($mapping), UserSessionClient::$SET_PAYMENT_TEMPLATE);

        $headers = array(
            'Cookie: guid=' . $token
        );

        $soap_url = Settings::$USER_PAYMENTS_URL;

        Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);
    }

    static function getScoringResult($token, $user_agent)
    {
        $soap_message = UserSessionClient::$GET_SCORING_TEMPLATE;
        $headers = array(
            'Cookie: guid=' . $token
        );
        $soap_url = Settings::$USER_SESSION_URL;

        $xml = Utils::soapRequest($soap_url, $user_agent, $headers, $soap_message);

        //XmlUtils.getXPathString(docElement, ".//*[local-name() = 'level']"),
        //XmlUtils.getXPathString(docElement, ".//*[local-name() = 'recommendation']"),
        //Integer.valueOf( XmlUtils.getXPathString(docElement, ".//*[local-name() = 'score']") )

        try {
            $level = null;
            $level_xml = $xml->xpath("//*[local-name()='level']");
            if (count($level_xml) > 0) {
                $level = strip_tags($level_xml[0]->asXML());
            }

            $recommendations = null;
            $recommendations_xml = $xml->xpath("//*[local-name()='recommendation']");
            if (count($recommendations_xml) > 0) {
                $recommendations = strip_tags($recommendations_xml[0]->asXML());
            }

            $score = null;
            $score_xml = $xml->xpath("//*[local-name()='score']");
            if (count($score_xml) > 0) {
                $score = strip_tags($score_xml[0]->asXML());
            }

            $scoringResult = new ScoringResult();
            $scoringResult->level = $level;
            $scoringResult->recommendations = $recommendations;
            $scoringResult->score = (int)$score;

            return $scoringResult;
        } catch (Exception $e) {
            throw new PassportExceptionImpl('Invalid server response');
        }
    }
}