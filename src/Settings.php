<?php

namespace Passport;

/**
 * Class Settings - �����, ���������� ��������� ����������
 * @package Passport
 */

class Settings
{
    public static $AUTH_URL = 'https://passport.pfma.ru/authorize';
    public static $TOKEN_URL = 'https://passport-api.pfma.ru/wsdl/BP_Auth/BP_Authorization_PortType';
    public static $CLIENT_AUTH_URL = 'https://passport-api.pfma.ru/wsdl/ClientAuth/ClientAuthPortType';
    public static $USER_SESSION_URL = 'https://passport-api.pfma.ru/wsdl/UserSession/UserSessionPortType';
    public static $USER_PAYMENTS_URL = 'https://passport-api.pfma.ru/wsdl/UserPayments/UserPaymentsPortType';
    public static $RISK_SCORING_URL = 'https://passport-api.pfma.ru/wsdl/RiskScoring/RiskScoringPortType';
    public static $WHOIS_URL = 'https://passport-api.pfma.ru/wsdl/WhoIsItService-2/WhoIsItServicePortType';
}
