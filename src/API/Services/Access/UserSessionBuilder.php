<?php

/**
 * ���� �������� ����� UserSessionBuilder
 */

namespace Passport\API\Services\Access;
use Passport\API\Exceptions\PassportException;

/**
 * ������������� �����, ����������� �������� ����������� ������������ � ��������
 * ���������������� ������ @see UserSession
 *
 * ������ ����� ���� ������ � ������� @see UserAccess::newBuilder()
 *
 * @package Passport\API
 */

interface UserSessionBuilder
{
    /**
     * ��������� URL ��� ��������������� �� ������ ��������������
     * ��� ������ ��������������
     * @return string
     */
    public function getRedirectUrl();

    /**
     * ���������� ���������������� ������ �� ������ ������ ���������� �� ������������,
     * �� ����������� ��� �� callback URL
     * @param $code - �������� ��������� 'code' � ������� �� callback URL
     * @param $state - �������� ��������� 'state' � ������� �� callback URL
     * @return UserSession
     * @throws PassportException
     */
    public function build($code, $state);
}