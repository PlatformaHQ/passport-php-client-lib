<?php

/**
 * ���� �������� ����� UserAccess
 */

namespace Passport\API\Services\Access;
use Passport\API\Common\UserData;
use Passport\API\Exceptions\PassportException;
use Passport\API\Common\Token;

/**
 * ������ ������� ��������� ����������������� ������������� � �������� ������ � �� ������
 *
 * ��������� ������ ����� �������� � ������� @see PassportClient::createUserAccess()
 *
 * ��� ����������� ������������ � ��������� ���������������� ������ (@see UserSession)
 * ���������� ��������� ��������� ����:
 *    1. ������� ��������� ������ UserSessionBuilder ����� ������ @see UserAccess::newBuilder()
 *    2. ��������� ��������� ������ UserSessionBuilder � http ������
 *    3. ������������� ������������ �� ������ ����������� �� @see UserSessionBuilder::getRedirectUrl()
 *    4. �� ����������� ������������ �� callback URL, ������� �������� ������ UserSessionBuilder �� http ������
 *    5. �������� ���������������� ������ ����� ������ @see UserSessionBuilder::build()
 *
 * @package Passport\API
 */

interface UserAccess
{
    /**
     * �������� �������������� �������, ������������� ��� ��������� ����������� ������������
     * @param string $callback_url - ������� URL ��� �������������� ������� � ����������, �������� "http://www.e-shop.ru/callback"
     * @param UserData $data - ������, �������������� �� ������ ������ ���������� �� �������
     * @return UserSessionBuilder
     * @throws PassportException
     */
    public function newBuilder($callback_url, $data);
}