<?php

/**
 * ���� �������� ����� UserSession
 */

namespace Passport\API\Services\Access;

use Passport\API\Common\Token;
use Passport\API\Exceptions\PassportException;
use Passport\API\Structures\PhoneInfo;
use Passport\API\Structures\ScoringResult;
use Passport\Internal\Clients\UserSessionClient;
use Passport\Internal\Exceptions\PassportExceptionImpl;
use Passport\Internal\Services\Access\UserSessionImpl;

/**
 * ������, ���������� ��� ������ � ������������
 *
 * ������ ����� ���� �������� ���� ����� @see UserSessionBuilder, ���� ����������� ������ @see UserAccess::find()
 *
 * @package Passport\API
 */
abstract class UserSession
{
    /**
     * ��������� ����� ������� � ������ ������������
     * ��� ������ ����� ����������� ������������, ���� ��������
     * @return Token
     */
    abstract public function getToken();

    /**
     * ��������� ����������� �������������� ������������
     * ��� ������ ����� �����������, ������������� �������� �������
     * @return string
     */
    abstract public function getUserId();

    /**
     * ��������� ������ �������� ������������
     * ����� ����� ������������� ������. ��������, 9520964300
     * ��� ���������� ����� ����������, ���� ������������ �� ��� ��������
     * @return string
     * @throws PassportException
     */
    abstract public function getPhone();

    /**
     * ����� ����������� ����� ������������
     * ��� ���������� ����� ���� ����������, ���� ������������ �� ��� ��������
     * @return string
     * @throws PassportException
     */
    abstract public function getEmail();

    /**
     * ���������� � ������ �������� ������������
     * ��� ���������� ����� ���� ����������, ���� ������������ �� ��� ��������
     * @return PhoneInfo
     * @throws PassportException
     */
    abstract public function getPhoneInfo();

    /*
     * ���������� ���������� �� ������
     * @return void
     * @throws PassportException
     */
    abstract public function setOrder($orderId, $price, $url = "empty");

    /**
     * ���������� ���������� �� �������
     * @return void
     * @throws PassportException
     */
    abstract public function setPayment($order, $payment, $user, $transaction, $price, $status="empty");

    /**
     * ���������� ������ ����� �������������
     * @return ScoringResult
     * @throws PassportException
     */
    abstract public function getScore();

    /**
     * ����� ���������������� ������ �� ����� �������
     * @param Token $token - ���� �������, ���������� � ���������������� ������
     * @param string $user_agent
     * @return UserSession
     * @throws PassportExceptionImpl
     */
    public static function find($token, $user_agent)
    {
        if (empty($token)) {
            throw new PassportExceptionImpl('token must be specified');
        }

        if (!is_a($token, "Passport\\API\\Common\\Token")) {
            throw new PassportExceptionImpl("Type of 'token' must be Passport\\API\\Common\\Token");
        }
        $user_id = UserSessionClient::findSession($token->getToken(), $user_agent);
        return new UserSessionImpl($user_agent, $token, $user_id);
    }
}