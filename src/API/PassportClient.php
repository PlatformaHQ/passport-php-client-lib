<?php

/**
 * ���� �������� ����� PassportClient
 */

namespace Passport\API;
use Passport\Internal\PassportClientImpl;
use Passport\API\Services\Access\UserAccess;

/**
 * ������� �������� - ��������� ����� ��� ������ � �����������
 *
 * ��� �������� �������� ���������� ����� ������������� � ������, �������� ������� �����,
 * ������������������� �� https://getpass.io
 *
 * @package Passport\API
 */

abstract class PassportClient
{
    /**
     * ��������� ���������� ������
     * @param $client_id - ������������� ����������, ������������� ����������
     * @return PassportClient
     */
    public static function getOne($client_id)
    {
        return new PassportClientImpl($client_id);
    }

    /**
     * �������� ������� �������
     * @param $client_secret - ������, �������� ��� ����������, ������������� ����������
     * @return UserAccess
     */
    public abstract function createUserAccess($client_secret);
}
