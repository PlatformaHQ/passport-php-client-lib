<?php

/**
 * ���� �������� ����� MobileSubscriberData
 */

namespace Passport\API\Structures;

/**
 * �������������� ���������� � ������ ��������
 *
 * @package Passport\API
 */
class MobileSubscriberData
{
    /**
     * Mobile Network Code
     * @var string
     */
    public $mnc;
    /**
     * ��� �������
     * @var string
     */
    public $geo_code;
    /**
     * Mobile Country Code
     * @var
     */
    public $mcc;
}