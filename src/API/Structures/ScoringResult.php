<?php

/**
 * ���� �������� ����� ScoringResult
 */

namespace Passport\API\Structures;

/**
 * �������� ������ ����� � ������� �������.
 * @package Passport\API
 */
class ScoringResult
{
    /**
     * ������������������� ������� ����� (high/medium/low)
     * the level of the risk
     * @var string
     */
    public $level;

    /**
     * ����������� �� ���������� �������� � �������������
     * recommendations for the user
     * @var string
     */
    public $recommendations;

    /**
     * ������ ����� � �������� ����
     * 0 - ����������� ����
     * 100 - ������������ ����
     * @var float
     */
    public $score;
}