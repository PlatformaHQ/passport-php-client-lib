<?php

/**
 * ���� �������� ����� PhoneInfo
 */

namespace Passport\API\Structures;

/**
 * ���������� � ������ ��������
 *
 * @package Passport\API
 */

class PhoneInfo
{
    /**
     * �������������� ���������� � ������ ��������
     * @var MobileSubscriberData
     */
    public $mobileSubscriberData;
    /**
     * ��� ��������� ��������� �����
     * @var string
     */
    public $operatorName;
    /**
     * �������� �������
     * @var string
     */
    public $regionName;
    /**
     * �������� ���������
     * @var string
     */
    public $settlementName;
    /**
     * ������� ���������� � �������������
     * @var boolean
     */
    public $isFraudSuspicion;
}