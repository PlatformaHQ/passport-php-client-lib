<?php

/**
 * ���� �������� ����� Token
 */

namespace Passport\API\Common;
use Passport\Internal\Common\TokenImpl;

/**
 * ���� ������� � ���������������� ������
 *
 * ����� �������������� ��� ��������� ������������ ���������������� ������ @see UserAccess::find()
 * �������� � ���������������� ������ @see UserSession
 *
 * @package Passport\API
 */
abstract class Token
{
    /**
     * ��������� ����� ������� � ��������� ����
     * @return string
     */
    public abstract function getToken();

    /**
     * ��������� ������� ��������� ����� �������
     * @return \DateTime
     */
    public abstract function getExpireDate();

    /**
     * �������� ���������� ������ �� ���������� ����
     * @param string $token_str
     * @return Token
     */
    public static function getOne($token_str)
    {
        return new TokenImpl($token_str);
    }
}