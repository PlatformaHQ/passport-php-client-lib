<?php

/**
 * ���� �������� ����� UserData
 */

namespace Passport\API\Common;
use Passport\Internal\Common\UserDataImpl;

/**
 * ������ ���������� �� ������� � ��������� 'user_data'
 * @package Passport\API
 */
abstract class UserData
{
    /**
     * �������� ���������� ������, �� ������ ���������� �� ��������� user_data, ����������� �� �������
     * @param $dataStr
     * @return UserData
     */
    public static function getOne($dataStr)
    {
        return new UserDataImpl($dataStr);
    }

    /**
     * �������� ���������� ������ ��� ������
     * @return string
     */
    public abstract function getData();
}