# README #

Клиентская библиотека для подключения к сервису Passport. 

Документация https://doc.getpass.io/developers/php/

Для генерации phpdoc в текущей директории вызвать

```
php composer.phar generate-docs
```